from dotenv import load_dotenv
import  os


load_dotenv()

APP_NAME = os.getenv('APP_NAME', 'Laravel Education REST')
BASE_URL = os.getenv('BASE_URL', 'http://education.local')
API_PART = os.getenv('API_PART', 'api')
API_URL = '{}/{}'.format(BASE_URL, API_PART)

AUTH_TOKEN = os.getenv('AUTH_TOKEN', None)
AUTH_TOKEN_TYPE = os.getenv('AUTH_TOKEN_TYPE', 'Bearer')