#!/usr/bin/python3
# -*- coding: utf-8 -*-
import settings
import requests


class Client:
    SC_OK = 200
    SC_UNAUTHORIZATION = 401
    __token = None

    
    # SINGLETON REALIZATION
    __instance = None

    def __init__(self):
        if not Client.__instance:
            Client.__token = settings.AUTH_TOKEN

    @classmethod
    def getInstance(cls):
        if not cls.__instance:
            cls.__instance = Client()
        return cls.__instance
    # END SINGLETON REALIZATION

    def get_auth_token(self):
        return '{} {}'.format(settings.AUTH_TOKEN_TYPE, self.__token)

    def request_headers(self):
        return {
            'Accept': 'application/json',
            'Authorization': self.get_auth_token()
        }

    @classmethod
    def get(cls, url:str, data:dict = None):
        response = requests.get(settings.API_URL + url, data=data, headers=cls.getInstance().request_headers())
        if response.status_code == cls.SC_UNAUTHORIZATION: raise UnauthorizationException(response)
        return response

    @classmethod
    def post(cls, url:str, data:dict = None):
        response = requests.post(settings.API_URL + url, data=data, headers=cls.getInstance().request_headers())
        if response.status_code == cls.SC_UNAUTHORIZATION: raise UnauthorizationException(response)
        return response

    @classmethod
    def put(cls, url:str, data:dict = None):
        response = requests.post(settings.API_URL + url, data=data.update({'_method': 'PUT'}), headers=cls.getInstance().request_headers())
        if response.status_code == cls.SC_UNAUTHORIZATION: raise UnauthorizationException(response)
        return response

    @classmethod
    def delete(cls, url:str, data:dict = None):
        response = requests.post(settings.API_URL + url, data=data.update({'_method': 'DELETE'}), headers=cls.getInstance().request_headers())
        if response.status_code == cls.SC_UNAUTHORIZATION: raise UnauthorizationException(response)
        return response

    @classmethod
    def auth(cls, login:str, password:str):
        response = cls.post('/auth', {'email': login, 'password': password})
        if response.ok:
            cls.__token = response.json()['token']
        else:
            response.raise_for_status()

    @classmethod
    def get_user(cls):
        return cls.get('/user').json()

    @classmethod
    def is_auth(cls):
        try:
            cls.get_user()
            return True
        except UnauthorizationException as e:
            return False
        else:
            return False


class UnauthorizationException(Exception):
    def __init__(self, response:requests.Response = None):
        super(UnauthorizationException, self).__init__()
        self.response = response
        

