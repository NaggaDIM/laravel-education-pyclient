#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLineEdit, QPushButton, QLabel
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtCore import Qt
import settings
from client import Client, UnauthorizationException


class MainWindow(QMainWindow):
	WINDOW_W = 640
	WINDOW_H = 480
	def __init__(self):
		super(MainWindow, self).__init__()
		self.setWindowTitle(settings.APP_NAME)
		self.update_widget(MainWidget(self) if Client.is_auth() else AuthWidget(self), 320, 120)
		if Client.is_auth(): self.update_widget(MainWidget(self))
		else: self.update_widget(AuthWidget(self), 320, 120)
		self.show()

	def update_widget(self, widget:QWidget, size_w:int = WINDOW_W, size_h:int = WINDOW_H):
		self.setFixedSize(size_w, size_h)
		self.center_on_screen()
		self.setCentralWidget(widget)

	def center_on_screen(self):
		resolution = QtWidgets.QDesktopWidget().screenGeometry()
		self.move((resolution.width() / 2) - (self.frameSize().width() / 2),
				(resolution.height() / 2) - (self.frameSize().height() / 2))


class MainWidget(QWidget):

	def __init__(self, window):
		super(MainWidget, self).__init__(window)
		self.window = window
		self.layout = QVBoxLayout()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		self.layout.addWidget(Header(self.window))
		self.layout.addWidget(QLabel('Some text'))
		self.layout.addStretch(1)


class AuthWidget(QWidget):

	def __init__(self, window):
		super(AuthWidget, self).__init__(window)
		self.window = window
		self.layout = QVBoxLayout()
		self.input_login = QLineEdit()
		self.input_password = QLineEdit()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		self.input_login.setPlaceholderText('E-Mail')
		self.input_login.setMinimumWidth(300)
		self.layout.addWidget(self.input_login)

		self.input_password.setPlaceholderText('Password')
		self.input_password.setEchoMode(QLineEdit.Password)
		self.layout.addWidget(self.input_password)

		self.layout.addStretch(1)

		buttons_layout = QHBoxLayout()

		button_login = QPushButton('Вход')
		button_login.clicked.connect(self.login)
		buttons_layout.addWidget(button_login)

		button_continue = QPushButton('Пропустить')
		button_continue.clicked.connect(self._continue)
		buttons_layout.addWidget(button_continue)

		self.layout.addLayout(buttons_layout)


	def login(self):
		try:
			Client.auth(self.input_login.text(), self.input_password.text())
			self.window.update_widget(MainWidget(self.window))
		except UnauthorizationException as e:
			pass

	def _continue(self):
		self.window.update_widget(MainWidget(self.window))


class Header(QWidget):
	def __init__(self, window):
		super(Header, self).__init__()
		self.window = window
		self.layout = QHBoxLayout()
		self.init_ui()
		self.setLayout(self.layout)

	def init_ui(self):
		self.layout.addWidget(QPushButton('Главная'))
		self.layout.addWidget(QPushButton('Каталог товаров'))
		self.layout.addWidget(QPushButton('Админ панель'))
		

		
